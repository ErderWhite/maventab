/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.maventab;

import static java.util.Arrays.sort;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author erderwhite
 */
public class MainTest {
    
     @Test
 public void test1(){
     Main instance = new Main(); 
     
     int arr[] = {7, 28, 14, 9, 1};
     
     int output[] = instance.tri_bulle(arr);
     
    
     Assertions.assertEquals(arr , output); 
 }
 
    @Test
 public void test2(){
     Main instance = new Main(); 
          
     
     int[] a1 = new int[0];
     int[] a2 = new int[0]; 
     a2 = instance.tri_bulle(a2);
         
     Assertions.assertArrayEquals(a1, a2);
     
 }
    
          @Test
 public void test3(){
     Main instance = new Main(); 
          
     
     int[] a1 = new int[]{1};
     int[] a2 = new int[]{1}; 
     a2 = instance.tri_bulle(a2);
         
     Assertions.assertArrayEquals(a1, a2);
     
 }
 
   
    
          @Test
 public void test4(){
     Main instance = new Main(); 
          
     
     int[] a1 = new int[]{1,5,10,28,41};
     int[] a2 = new int[]{1,5,10,28,41}; 
     a2 = instance.tri_bulle(a2);
         
     Assertions.assertArrayEquals(a1, a2);
     
 }
             @Test
 public void test5(){
     Main instance = new Main(); 
          
     
     int[] a1 = new int[]{-10,-5,5,7,19};
     int[] a2 = new int[]{5,-5,7,-10,19}; 
     a2 = instance.tri_bulle(a2);
         
     Assertions.assertArrayEquals(a1, a2);
     
 }
    
}
