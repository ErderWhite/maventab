/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.maventab;

/**
 *
 * @author erderwhite
 */
public class Main {
    
    
    
    public static int[] tri_bulle(int[] tab)
   {  
        int taille = tab.length;  
        int tmp = 0;  
        for(int i=0; i < taille; i++) 
        {
                for(int j=1; j < (taille-i); j++)
                {  
                        if(tab[j-1] > tab[j])
                        {
                                //echanges des elements
                                tmp = tab[j-1];  
                                tab[j-1] = tab[j];  
                                tab[j] = tmp;  
                        }
 
                }
        }
        
        return tab;
   }

    
}
